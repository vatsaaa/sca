# -*- coding: utf-8 -*-

import os
import sys
import getopt
import pygount
import file_extensions
from file_extensions import *


def source_code_scanner(analyse_location):
    file_path = []
    code_line = []
    dirlist = ["venv", "__pycache__", ".idea"]

    for (dirpath, dirnames, filenames) in os.walk(analyse_location):
        if any(x in dirpath for x in dirlist):
            continue

        for files in filenames:
            location = os.path.join(dirpath, files)
            file_path.append(location)

    total_lines = 0

    for file_type in file_extensions.keys():
        sum_of_lines = 0

        for path in range(len(file_path)):
            if isinstance(file_extensions[file_type], list):
                for extension in file_extensions[file_type]:
                    if extension in str(file_path[path]):
                        analysis = pygount.source_analysis(file_path[path], 'pygount')
                        sum_of_lines += analysis.code
            elif file_extensions[file_type] in str(file_path[path]):
                analysis = pygount.source_analysis(file_path[path], 'pygount')
                sum_of_lines += analysis.code

        total_lines = total_lines + sum_of_lines
        code_line.append(sum_of_lines)

        if sum_of_lines != 0:
            print ('%s: %s line(s) of code' % (file_type, sum_of_lines))

    print ('\n')
    file_types = []

    for languages in file_extensions.keys():
        file_types.append(languages)

    language_percent = {}

    for i in range(len(code_line)):
        if total_lines:
            percentage = code_line[i] / total_lines * 100
            language_percent[file_types[i]] = percentage
        else:
            percentage = 0

        if percentage != 0:
            print('Percentage of %s : %.3f %s ' % (file_types[i], percentage, '%'))

    popular_lang = max(language_percent, key=language_percent.get)
    return "You are most proficient in " + popular_lang + " programming language"


def usage():
    print("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-")
    print("Usage: ")
    print("-h --help" + ": Print this help message")
    print("-d --dir=" + ": [python ]source_scanner.py -d <code directory to scan>")
    return


def process_args(args):
    prj_dir = ""
    try:
        opts, args = getopt.getopt(args, "hd:", ["help", "dir="])
    except getopt.GetoptError as err:
        print("Error: " + str(err))
        usage()
        sys.exit(2)

    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-d", "--dir"):
            prj_dir = a
        else:
            assert False, "Invalid option: " + o + "provided!"

    return prj_dir


def main(args):
    return print(source_code_scanner(process_args(args)))


if __name__ == '__main__':
    main(sys.argv[1:])
